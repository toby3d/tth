package internal

import (
	"fmt"
	"path/filepath"
	"strings"

	"github.com/lunny/html2md"
)

func HtmlToMarkdown(src string) string {
	src = strings.NewReplacer(
		"&rsquo;", "'",
		"&ldquo;", `"`,
		"&rdquo;", `"`,
		"&hellip;", "...",
		"“", `"`,
		"”", `"`,
	).Replace(src)

	var img, vid, aud int
	html2md.AddRule("img", &html2md.Rule{
		Patterns: html2md.Img().Patterns,
		Tp:       html2md.Img().Tp,
		Replacement: func(innerHTML string, attrs []string) string {
			attrs = strings.Split(attrs[0][1:], " ")
			name := attrs[0]
			ext := filepath.Ext(name)

			var filename string
			switch ext[1:4] {
			case "png", "jpg", "gif":
				filename = fmt.Sprint("image_", img, ext)
			case "mp4":
				filename = fmt.Sprint("video_", vid, ext)
			case "mp3":
				filename = fmt.Sprint("audio_", aud, ext)
			}

			return fmt.Sprintf("![](%s)", filename)
		},
	})

	return html2md.Convert(src)
}
