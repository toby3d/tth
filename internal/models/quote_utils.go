package models

import (
	"fmt"
	"strings"

	"gitlab.com/toby3d/tth/internal"
)

func (q *Quote) HasSource() bool {
	return q != nil && q.Source != ""
}

func (q *Quote) Title() string {
	return ""
}

func (q *Quote) Body() (body string) {
	body = "> " + internal.HtmlToMarkdown(q.Text)
	strings.Replace(body, "\n", "\n> ", -1)
	if q.HasSource() {
		body = fmt.Sprint(body, "\n\n", "-- ", internal.HtmlToMarkdown(q.Source))
	}
	return
}
