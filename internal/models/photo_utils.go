package models

import (
	"fmt"

	"gitlab.com/toby3d/tth/internal"
)

func (p *PhotoSize) String(caption string) string {
	return fmt.Sprint("![", caption, "](", p.Url, ")")
}

func (pp *PhotosetPhoto) String() string {
	if pp == nil || len(pp.Photo) == 0 {
		return ""
	}

	return pp.Photo[0].String(pp.Caption)
}

func (p *Photo) IsSet() bool {
	return p != nil && len(p.Photoset) > 0
}

func (p *Photo) HasLink() bool {
	return p != nil && p.LinkUrl.URI != nil
}

func (p *Photo) Title() string {
	return ""
}

func (p *Photo) Body() string {
	switch {
	case p.IsSet():
		var body string
		for _, photo := range p.Photoset {
			body = fmt.Sprint(body, photo.String())
		}
		return fmt.Sprint(
			body, "\n", "\n",
			internal.HtmlToMarkdown(p.Caption),
		)
	case p.HasLink():
		return fmt.Sprint(
			fmt.Sprint("[", p.Photo[0].String(""), "](", p.LinkUrl.String(), ")"), "\n",
			internal.HtmlToMarkdown(p.Caption),
		)
	default:
		return fmt.Sprint(
			p.Photo[0].String(""), "\n",
			internal.HtmlToMarkdown(p.Caption),
		)
	}
}
