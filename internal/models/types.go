package models

import (
	"encoding/xml"
	"strconv"
	"time"

	http "github.com/valyala/fasthttp"
)

type (
	Time struct{ time.Time }
	URL  struct{ *http.URI }

	Tumblr struct {
		XMLName xml.Name `xml:"tumblr"`
		Version string   `xml:"version,attr"`
		Posts   []Post   `xml:"posts>post"`
	}

	Post struct {
		ID            int64  `xml:"id,attr"`             // 137179991314
		Url           URL    `xml:"url,attr"`            // https://blog.toby3d.ru/post/137179991314
		UrlWithSlug   URL    `xml:"url-with-slug,attr"`  // https://blog.toby3d.ru/post/137179991314/1-год-с-днём-рождения-элизабет
		Type          string `xml:"type,attr"`           // regular
		DateGMT       Time   `xml:"date-gmt,attr"`       // 2009-06-01 10:08:00 GMT
		Date          Time   `xml:"date,attr"`           // Fri, 01 Apr 2011 05:00:00
		UnixTimestamp Time   `xml:"unix-timestamp,attr"` // 1243850880
		Format        string `xml:"format,attr"`         // html
		ReblogKey     string `xml:"reblog-key,attr"`     // 01Tsqd1Y
		Slug          string `xml:"slug,attr"`           // 1-год-с-днём-рождения-элизабет
		State         string `xml:"state,attr"`          // draft
		IsReblog      bool   `xml:"is_reblog,attr"`      // false
		Tumblelog     string `xml:"tumblelog,attr"`      // toby3d
		Private       bool   `xml:"private,attr"`        // true

		*Answer
		*Audio
		*Conversation
		*Link
		*Photo
		*Quote
		*Regular
		*Video

		Tags []string `xml:"tag"` // telegram text russian
	}

	Answer struct {
		Question string `xml:"question"`
		Answer   string `xml:"answer"`
	}

	Audio struct {
		Plays int `xml:"audio-plays,attr"` // 349

		Caption   string `xml:"audio-caption"`
		Embed     string `xml:"audio-embed"`
		ID3Album  string `xml:"id3-album"`
		ID3Artist string `xml:"id3-artist"`
		ID3Title  string `xml:"id3-title"`
		Player    string `xml:"audio-player"`
	}

	Conversation struct {
		Title string `xml:"conversation-title"`
		Text  string `xml:"conversation-text"`

		Lines []Line `xml:"conversation>line"`
	}

	Line struct {
		Name  string `xml:"name,attr"`
		Label string `xml:"label,attr"`

		Line string `xml:"line"`
	}

	Link struct {
		Description string `xml:"link-description"`
		Text        string `xml:"link-text"`
		Url         URL    `xml:"link-url"`
	}

	Photo struct {
		Width  int `xml:"width,attr"`  // 1000
		Height int `xml:"height,attr"` // 1000

		Caption string `xml:"photo-caption"`
		LinkUrl URL    `xml:"photo-link-url"`

		Photo    []PhotoSize     `xml:"photo-url"`
		Photoset []PhotosetPhoto `xml:"photoset>photo"`
	}

	PhotosetPhoto struct {
		Caption string `xml:"caption,attr"`
		Height  string `xml:"height,attr"`
		Offset  string `xml:"offset,attr"`
		Width   string `xml:"width,attr"`

		Photo []PhotoSize `xml:"photo-url"`
	}

	PhotoSize struct {
		MaxWidth int `xml:"max-width,attr"`

		Url string `xml:",chardata"`
	}

	Quote struct {
		Text   string `xml:"quote-text"`
		Source string `xml:"quote-source"`
	}

	Regular struct {
		Title string `xml:"regular-title"`
		Body  string `xml:"regular-body"`
	}

	Video struct {
		DirectVideo bool `xml:"direct-video,attr"` // true

		Caption string        `xml:"video-caption"`
		Player  []VideoPlayer `xml:"video-player"`
		Source  VideoSource   `xml:"video-source"`
	}

	VideoPlayer struct {
		MaxWidth int    `xml:"max-width,attr"`
		Code     string `xml:",chardata"`
	}

	VideoSource struct {
		ContentType string `xml:"content-type"`
		Extension   string `xml:"extension"`
		Width       int    `xml:"width"`
		Height      int    `xml:"height"`
		Duration    int    `xml:"duration"`
		Revision    int    `xml:"revision"`
	}

	Reader interface {
		Title() string
		Body() string
	}
)

const (
	TypeAnswer       = "answer"
	TypeAudio        = "audio"
	TypeConversation = "conversation"
	TypeLink         = "link"
	TypePhoto        = "photo"
	TypeQuote        = "quote"
	TypeRegular      = "regular"
	TypeVideo        = "video"

	StateDraft     = "draft"
	StatePrivate   = "private"
	StatePublished = "published"

	FormatHTML     = "html"
	FormatMarkdown = "markdown"

// time.RFC3339
)

func (t *Time) UnmarshalXMLAttr(attr xml.Attr) (err error) {
	switch attr.Name.Local {
	case "date-gmt":
		t.Time, err = time.Parse("2006-01-02 15:04:05 MST", attr.Value)
	case "date":
		t.Time, err = time.Parse("Mon, 02 Jan 2006 15:04:05", attr.Value)
	case "unix-timestamp":
		var ts int64
		if ts, err = strconv.ParseInt(attr.Value, 10, 64); err != nil {
			return
		}

		t.Time = time.Unix(ts, 0)
	}
	return
}

func (u *URL) UnmarshalXMLAttr(attr xml.Attr) (err error) {
	if attr.Value == "" {
		return nil
	}

	u.URI = http.AcquireURI()
	u.URI.Update(attr.Value)
	return
}

func (u *URL) UnmarshalXML(d *xml.Decoder, start xml.StartElement) (err error) {
	var raw string
	if err = d.DecodeElement(&raw, &start); err != nil {
		return
	}

	u.URI = http.AcquireURI()
	u.URI.Update(raw)
	return
}
