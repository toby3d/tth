package models

import (
	"fmt"
	"strings"

	"gitlab.com/toby3d/tth/internal"
)

func (a *Answer) Title() string {
	return strings.Replace(a.Question, `"`, `\"`, -1)
}

func (a *Answer) Body() string {
	return fmt.Sprintln(
		">", a.Question, "\n",
		internal.HtmlToMarkdown(a.Answer),
	)
}
