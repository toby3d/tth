package models

import (
	"fmt"
	"strings"

	"gitlab.com/toby3d/tth/internal"
)

func (a *Audio) HasTitle() bool {
	return a != nil && a.ID3Title != ""
}

func (a *Audio) HasArtist() bool {
	return a != nil && a.ID3Artist != ""
}

func (a *Audio) HasAlbum() bool {
	return a != nil && a.ID3Album != ""
}

func (a *Audio) HasCaption() bool {
	return a != nil && a.Caption != ""
}

func (a *Audio) Title() (title string) {
	if !a.HasTitle() {
		return
	}

	title = strings.Replace(a.ID3Title, `"`, `\"`, -1)

	if a.HasArtist() {
		title = strings.Replace(a.ID3Artist, `"`, `\"`, -1) + " - " + title
	}

	return
}

func (a *Audio) Body() (body string) {
	body = a.Player

	if a.HasCaption() {
		body = fmt.Sprint(
			body, "\n\n",
			internal.HtmlToMarkdown(a.Caption),
		)
	}

	return
}
