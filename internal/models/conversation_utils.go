package models

import "fmt"

func (l *Line) String() string {
	return fmt.Sprintf("%s: %s\n", l.Name, l.Line)
}

func (c *Conversation) Body() (body string) {
	for _, line := range c.Lines {
		body = fmt.Sprintln(body, line.String(), "\n")
	}

	return
}
