package models

import (
	"fmt"
	"strings"

	"gitlab.com/toby3d/tth/internal"
)

func (l *Link) Title() string {
	return strings.Replace(l.Text, `"`, `\"`, -1)
}

func (l *Link) Body() string {
	return fmt.Sprintf(
		"# [%s](%s) #\n\n%s",
		l.Text, l.Url.String(),
		internal.HtmlToMarkdown(l.Description),
	)
}
