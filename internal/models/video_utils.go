package models

import (
	"fmt"

	"gitlab.com/toby3d/tth/internal"
)

func (v *Video) Title() string {
	return ""
}

func (v *Video) Body() (body string) {
	return fmt.Sprint(v.Player[0].Code, "\n", "\n",
		internal.HtmlToMarkdown(v.Caption),
	)
}
