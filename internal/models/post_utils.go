package models

import (
	"fmt"
	"path"
	"strconv"
	"strings"
	"time"

	"gitlab.com/toby3d/tth/internal"
)

func (p *Post) IsAnswer() bool {
	return p != nil && p.Type == TypeAnswer && p.Answer != nil
}

func (p *Post) IsAudio() bool {
	return p != nil && p.Type == TypeAudio && p.Audio != nil
}

func (p *Post) IsConversation() bool {
	return p != nil && p.Type == TypeConversation && p.Conversation != nil
}

func (p *Post) IsLink() bool {
	return p != nil && p.Type == TypeLink && p.Link != nil
}

func (p *Post) IsPhoto() bool {
	return p != nil && p.Type == TypePhoto && p.Photo != nil
}

func (p *Post) IsQuote() bool {
	return p != nil && p.Type == TypeQuote && p.Quote != nil
}

func (p *Post) IsRegular() bool {
	return p != nil && p.Type == TypeRegular && p.Regular != nil
}

func (p *Post) IsVideo() bool {
	return p != nil && p.Type == TypeVideo && p.Video != nil
}

func (p *Post) IsDraft() bool {
	return p != nil && p.State == StateDraft
}

func (p *Post) IsPrivate() bool {
	return p != nil && p.State == StatePrivate
}

func (p *Post) IsPublished() bool {
	return p != nil && p.State == StatePublished
}

func (p *Post) IsHTML() bool {
	return p != nil && p.Format == FormatHTML
}

func (p *Post) IsMarkdown() bool {
	return p != nil && p.Format == FormatMarkdown
}

func (p *Post) HasSlug() bool {
	return p != nil && p.Slug != ""
}

func (p *Post) HasTags() bool {
	return p != nil && len(p.Tags) > 0
}

func (p *Post) FormatTitle() string {
	var post Reader
	switch p.Type {
	case TypeAnswer:
		post = p.Answer
	case TypeAudio:
		post = p.Audio
	case TypeConversation:
		return fmt.Sprintln("title:", `"`+strings.Replace(p.Conversation.Title, `"`, `\"`, -1)+`"`)
	case TypeLink:
		post = p.Link
	case TypePhoto:
		post = p.Photo
	case TypeQuote:
		post = p.Quote
	case TypeRegular:
		return fmt.Sprintln("title:", `"`+strings.Replace(p.Regular.Title, `"`, `\"`, -1)+`"`)
	case TypeVideo:
		post = p.Video
	default:
		return ""
	}

	return fmt.Sprintln("title:", `"`+post.Title()+`"`)
}

func (p *Post) FormatBody() string {
	var post Reader
	switch p.Type {
	case TypeAnswer:
		post = p.Answer
	case TypeAudio:
		post = p.Audio
	case TypeConversation:
		return internal.HtmlToMarkdown(p.Conversation.Body())
	case TypeLink:
		post = p.Link
	case TypePhoto:
		post = p.Photo
	case TypeQuote:
		post = p.Quote
	case TypeRegular:
		return internal.HtmlToMarkdown(p.Regular.Body)
	case TypeVideo:
		post = p.Video
	default:
		return ""
	}

	return post.Body()
}

func (p *Post) FormatSlug() string {
	return fmt.Sprintln("slug:", p.Slug)
}

func (p *Post) FormatDate() string {
	return fmt.Sprintln("date:", p.Date.Format(time.RFC3339))
}

func (p *Post) FormatDraft() string {
	return fmt.Sprintln("draft:", (p.IsDraft() || p.IsPrivate()))
}

func (p *Post) FormatType() string {
	return fmt.Sprintln("type:", p.Type)
}

func (p *Post) FormatAliases() (body string) {
	body = fmt.Sprintln(
		"aliases:\n",
		" -", path.Join("/post", strconv.FormatInt(p.ID, 10)),
	)
	if p.HasSlug() {
		body = fmt.Sprintln(body,
			" -", path.Join("/post", strconv.FormatInt(p.ID, 10), p.Slug),
		)
	}
	return
}

func (p *Post) FormatTags() string {
	return fmt.Sprintln(
		"tags:\n",
		" -", strings.Join(p.Tags, "\n  - "),
	)
}

func (p *Post) FormatMedia(media ...string) string {
	if len(media) == 0 {
		return ""
	}

	return fmt.Sprintln(
		"resources:\n",
		" - src:", strings.Join(media, "\n  - src: "),
	)
}
