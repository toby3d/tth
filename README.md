# tumblr-to-hugo
## HowTo
1. You need export your blog first:
  + Go to [settings](https://www.tumblr.com/settings/account);
  + Select your blog in right sidebar;
  + Scroll to "Export" section;
  + Request an archive with your blog content;
  + Wait several hours (or days), download and unzip your archive;
2. Use `tth` with this flags: `tth -posts=./posts/posts.xml -media=./media/ -dst=.../my-hugo-site/content/blog/`
3. ?????????
4. PROFIT!!1

## Settings
You can customize output by this optional flags:
- `-a`: generate Answer posts only
- `-m`: generate Audio (\[m\]usic) posts only
- `-c`: generate Conversation posts only
- `-l`: generate Link posts only
- `-p`: generate Photo posts only
- `-q`: generate Quote posts only
- `-t`: generate Regular posts only
- `-v`: generate Video posts only
- `-private`: generate drafts and private posts too
- `-reblog`: generate reblogs posts too
