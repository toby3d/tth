package main

import (
	"encoding/xml"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/toby3d/tth/internal/models"
)

var (
	flagPosts       = flag.String("posts", filepath.Join(".", "posts.xml"), "path to posts.xml source")
	flagMedia       = flag.String("media", filepath.Join(".", "media/"), "path to media folder")
	flagDestination = flag.String("dst", filepath.Join(".", "content", "post"), "path to content/ hugo folder")

	flagAnswer       = flag.Bool("a", false, "generate only answer posts")
	flagAudio        = flag.Bool("m", false, "generate only audio (music) posts")
	flagConversation = flag.Bool("c", false, "generate only conversation posts")
	flagLink         = flag.Bool("l", false, "generate only link posts")
	flagPhoto        = flag.Bool("p", false, "generate only photo posts")
	flagQuote        = flag.Bool("q", false, "generate only quote posts")
	flagRegular      = flag.Bool("t", false, "generate only regular posts")
	flagVideo        = flag.Bool("v", false, "generate only video posts")

	flagPrivate = flag.Bool("private", false, "generate drafts and/or private posts")
	flagReblog  = flag.Bool("reblog", false, "generate reblogs posts")
	flagID      = flag.Bool("title-id", false, "generate posts folders by it's id")
	flagSlug    = flag.Bool("title-slug", true, "generate posts folders by it's slug (if exist)")
)

func errCheck(err error) {
	if err == nil {
		return
	}

	panic(err.Error())
}

func init() {
	log.Println("starting...")
}

func main() {
	flag.Parse()

	srcPosts, err := filepath.Abs(*flagPosts)
	errCheck(err)

	srcMedia, err := filepath.Abs(*flagMedia)
	errCheck(err)

	dstPath, err := filepath.Abs(*flagDestination)
	errCheck(err)

	body, err := ioutil.ReadFile(srcPosts)
	errCheck(err)

	var tumblr models.Tumblr
	err = xml.Unmarshal(body, &tumblr)
	errCheck(err)

	for _, post := range tumblr.Posts {
		if blockType(post.Type) ||
			(post.IsPrivate() && !*flagPrivate) ||
			(post.IsReblog && !*flagReblog) {
			continue
		}

		postDir := dstPath
		switch {
		case (!*flagID && !*flagSlug) || (*flagID && *flagSlug):
			*flagID = true
		case *flagSlug:
			if post.HasSlug() {
				postDir = filepath.Join(postDir, post.Slug)
			} else {
				postDir = filepath.Join(postDir, strconv.FormatInt(post.ID, 10))
			}
		case *flagID:
			postDir = filepath.Join(postDir, strconv.FormatInt(post.ID, 10))
		}

		if err = CreateDirIfNotExist(postDir); err != nil {
			log.Println("error:", err.Error())
			continue
		}

		media, err := MoveMediaIfExist(srcMedia, postDir, post.ID)
		if err != nil {
			log.Println("error:", err.Error())
			continue
		}

		// Create new post file
		f, err := os.Create(filepath.Join(postDir, "index.md"))
		if err != nil {
			log.Println("error:", err.Error())
			continue
		}
		defer f.Close()

		f.WriteString("---\n")              // Open post settings section
		f.WriteString(post.FormatDraft())   // draft: true
		f.WriteString(post.FormatTitle())   // title: "some title"
		f.WriteString(post.FormatDate())    // date: 2018-07-21T20:18:00+05:00
		f.WriteString(post.FormatType())    // type: regular
		f.WriteString(post.FormatSlug())    // slug: example-slug
		f.WriteString(post.FormatAliases()) // aliases: ["/post/12345", "/post/12345/example-slug"]
		f.WriteString(post.FormatTags())    // tags: ["example", "post", "tag"]
		f.WriteString(post.FormatMedia(media...))
		f.WriteString("---\n") // End post settings section
		f.WriteString(post.FormatBody())
	}
}

func blockType(t string) bool {
	switch {
	case !*flagAnswer && !*flagAudio &&
		!*flagConversation && !*flagLink &&
		!*flagPhoto && !*flagQuote &&
		!*flagRegular && !*flagVideo:
		return false
	case t == models.TypeAnswer && !*flagAnswer,
		t == models.TypeAudio && !*flagAudio,
		t == models.TypeConversation && !*flagConversation,
		t == models.TypeLink && !*flagLink,
		t == models.TypePhoto && !*flagPhoto,
		t == models.TypeQuote && !*flagQuote,
		t == models.TypeRegular && !*flagRegular,
		t == models.TypeVideo && !*flagVideo:
		return true
	default:
		return false
	}
}

func CreateDirIfNotExist(dir string) (err error) {
	if _, err = os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
	}
	return
}

func MoveMediaIfExist(oldDir, newDir string, id int64) ([]string, error) {
	var media []string

	prefix := strconv.FormatInt(id, 10)
	return media, filepath.Walk(oldDir, func(path string, info os.FileInfo, err error) error {
		if err != nil || info.IsDir() || !strings.HasPrefix(info.Name(), prefix) {
			return err
		}

		suffix := strings.TrimPrefix(info.Name(), prefix)
		ext := filepath.Ext(suffix)

		var filename string
		switch ext[1:] {
		case "png", "jpg", "gif":
			filename = "image"
		case "mp4":
			filename = "video"
		case "mp3":
			filename = "audio"
		}
		filename += suffix

		media = append(media, filename)

		input, err := ioutil.ReadFile(filepath.Join(oldDir, info.Name()))
		if err != nil {
			return err
		}

		return ioutil.WriteFile(filepath.Join(newDir, filename), input, 0644)
	})
}

func copy(src, dst string) {

}
